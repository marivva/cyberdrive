//
//  CDTabBarNavigationController.swift
//  CyberDrive
//
//  Created by Marivva on 18.12.2021.
//

import UIKit

final class CDTabBarNavigationController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureItems()
    }

    private func configureItems() {
        let searchItem = SearchViewController()
        let carTrackerItem = CarTrackerViewController(presenter: CarTrackerPresenter())
        let profileItem = ProfileViewController()
        
        let searchImage = UIImage(named: "search")?.withRenderingMode(.alwaysOriginal)
        let carImage = UIImage(named: "car")?.withRenderingMode(.alwaysOriginal)
        let profileImage = UIImage(named: "profile")?.withRenderingMode(.alwaysOriginal)
        
        let searchIcon = UITabBarItem(title: "Search",
                                      image: searchImage,
                                      selectedImage: searchImage)
        searchIcon.tag = TabBarItems.search.rawValue
        
        let carTrackerIcon = UITabBarItem(title: "Tracker",
                                          image: carImage,
                                          selectedImage: carImage)
        carTrackerIcon.tag = TabBarItems.tracker.rawValue
        
        let profileIcon = UITabBarItem(title: "Profile",
                                       image: profileImage,
                                       selectedImage: profileImage)
        profileIcon.tag = TabBarItems.profile.rawValue
        
        searchItem.tabBarItem = searchIcon
        carTrackerItem.tabBarItem = carTrackerIcon
        profileItem.tabBarItem = profileIcon
        
        let controllers = [searchItem, carTrackerItem, profileItem]
        viewControllers = controllers.map { UINavigationController(rootViewController: $0) }
        selectedIndex = 1
    }
}
