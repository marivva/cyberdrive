//
//  Colors.swift
//  CyberDrive
//
//  Created by Gala on 18.12.2021.
//

import UIKit

enum Colors {
    static let backgroundColor = UIColor(hex24: 0x1A2133)
    static let tintColor = UIColor(hex24: 0x7C69C4)
    static let nonAccentColor = UIColor(hex24: 0x7879F1)
    static let textColor = UIColor(hex24: 0xA5A6F6)
    static let notificationCellColor = UIColor(hex24: 0xC4C4C4).withAlphaComponent(0.1)
}

extension UIColor {
    convenience init(hex24 int: UInt32) {
        let a, r, g, b: UInt32
        (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}
