//
//  CheckBox.swift
//  CyberDrive
//
//  Created by Gala on 18.12.2021.
//

import UIKit

class CheckBox: UIButton {
    // Images
    private let checkedImage = UIImage(named: "check")
    
    var didChangeStatus: ((Bool) -> Void)?
    
    var isChecked: Bool = false {
        didSet {
            setImage(isChecked ? checkedImage : .none, for: .normal)
        }
    }
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.borderWidth = 1
        layer.borderColor = UIColor.white.cgColor
        addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
        isChecked = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func didTapButton() {
        isChecked = !isChecked
        didChangeStatus?(isChecked)
    }
}
