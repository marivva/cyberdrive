//
//  SearchViewController.swift
//  CyberDrive
//
//  Created by Maha on 18.12.2021.
//

import UIKit

final class SearchViewController: UIViewController {

    // TODO:
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Colors.backgroundColor
        navigationController?.tabBarController?.tabBar.backgroundColor = Colors.backgroundColor
        navigationController?.tabBarController?.tabBar.tintColor = Colors.textColor
    }
}

