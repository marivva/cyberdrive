//
//  NotificationsPickerTableViewCell.swift
//  CyberDrive
//
//  Created by Gala on 18.12.2021.
//

import UIKit

final class NotPickerTableViewCell: UITableViewCell {
    
    static let identifier = String(describing: self)
    
    // TODO переделать на кастомные кнопки
    private let currentNotification: UILabel = {
        let label = UILabel()
        label.textColor = Colors.textColor
        label.text = "Current notifications"
        return label
    }()
    
    private let addNotification: UILabel = {
        let label = UILabel()
        label.textColor = Colors.textColor
        label.text = "Add notification"
        return label
    }()
    
    private let currentTwoLinesView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.textColor
        return view
    }()
    
    private let addSingleLineView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.textColor
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureSubviews()
        addConstraints()
        selectionStyle = .none
        contentView.isUserInteractionEnabled = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureSubviews() {
        backgroundColor = Colors.backgroundColor
        contentView.addSubview(currentTwoLinesView)
        contentView.addSubview(addSingleLineView)
        contentView.addSubview(currentNotification)
        contentView.addSubview(addNotification)
    }
    
    private func addConstraints() {
        addSingleLineView.translatesAutoresizingMaskIntoConstraints = false
        currentTwoLinesView.translatesAutoresizingMaskIntoConstraints = false
        addNotification.translatesAutoresizingMaskIntoConstraints = false
        currentNotification.translatesAutoresizingMaskIntoConstraints = false
        
        
        NSLayoutConstraint.activate([
            currentNotification.topAnchor.constraint(equalTo: topAnchor, constant: 2),
            currentNotification.leftAnchor.constraint(equalTo: leftAnchor, constant: 8),
            
            addNotification.topAnchor.constraint(equalTo: topAnchor, constant: 2),
            addNotification.rightAnchor.constraint(equalTo: rightAnchor, constant: -5),
            
            currentTwoLinesView.heightAnchor.constraint(equalToConstant: 5),
            currentTwoLinesView.widthAnchor.constraint(equalTo: currentNotification.widthAnchor, constant: 8),
            currentTwoLinesView.leftAnchor.constraint(equalTo: leftAnchor),
            currentTwoLinesView.topAnchor.constraint(equalTo: currentNotification.bottomAnchor, constant: 2),
            
            addSingleLineView.heightAnchor.constraint(equalToConstant: 3),
            addSingleLineView.widthAnchor.constraint(equalTo: addNotification.widthAnchor, constant: 5),
            addSingleLineView.rightAnchor.constraint(equalTo: rightAnchor),
            addSingleLineView.topAnchor.constraint(equalTo: addNotification.bottomAnchor, constant: 2)
        ])
    }
    
}
