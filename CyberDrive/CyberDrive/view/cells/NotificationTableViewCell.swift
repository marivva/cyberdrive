//
//  NotificationTableViewCell.swift
//  CyberDrive
//
//  Created by Gala on 18.12.2021.
//

import UIKit

final class NotificationTableViewCell: UITableViewCell {
    
    static let identifier = String(describing: self)
    
    private let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        return formatter
    }()
    
    private let dateLabel: UILabel = {
        let label = UILabel()
        label.textColor = Colors.textColor
        return label
    }()
    
    private let messageLabel: UILabel = {
        let label = UILabel()
        label.textColor = Colors.textColor
        return label
    }()
    
    private let checkBox = CheckBox()
    
    private let backView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.layer.borderColor = Colors.nonAccentColor.cgColor
        view.layer.borderWidth = 2
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureSubviews()
        addConstraints()
        selectionStyle = .none
        contentView.isUserInteractionEnabled = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(with model: NotificationModel, statusChangedHandler: ((Bool) -> Void)? = nil) {
        dateLabel.text = formatter.string(from: model.notificationDate)
        messageLabel.text = model.notificationMessage
        checkBox.isChecked = model.notificationStatus
        checkBox.didChangeStatus = statusChangedHandler
    }
    
    private func configureSubviews() {
        backgroundColor = Colors.notificationCellColor
        contentView.addSubview(backView)
        contentView.addSubview(dateLabel)
        contentView.addSubview(messageLabel)
        contentView.addSubview(checkBox)
    }
    
    private func addConstraints() {
        backView.translatesAutoresizingMaskIntoConstraints = false
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        checkBox.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            backView.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            backView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
            backView.rightAnchor.constraint(equalTo: rightAnchor, constant: -10),
            backView.leftAnchor.constraint(equalTo: leftAnchor, constant: 10),
            
            dateLabel.topAnchor.constraint(equalTo: backView.topAnchor, constant: 10),
            dateLabel.leftAnchor.constraint(equalTo: backView.leftAnchor, constant: 10),
            
            messageLabel.topAnchor.constraint(equalTo: dateLabel.bottomAnchor, constant: 10),
            messageLabel.leftAnchor.constraint(equalTo: backView.leftAnchor, constant: 10),
            
            checkBox.bottomAnchor.constraint(equalTo: backView.bottomAnchor, constant: -10),
            checkBox.rightAnchor.constraint(equalTo: backView.rightAnchor, constant: -10),
            checkBox.heightAnchor.constraint(equalToConstant: 32),
            checkBox.widthAnchor.constraint(equalToConstant: 32)
        ])
    }
}
