//
//  CarInfoTableViewCell.swift
//  CyberDrive
//
//  Created by Maha on 18.12.2021.
//

import UIKit

final class CarInfoTableViewCell: UITableViewCell {
    
    static let identifier = String(describing: self)
    
    private let editButton: UIButton = {
        let button = UIButton()
        button.setTitle("Edit info", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = Colors.tintColor
        button.layer.cornerRadius = 15
        return button
    }()
    
    private let carImage = UIImageView()
    
    private let carName: UILabel = {
        let label = UILabel()
        label.textColor = Colors.textColor
        return label
    }()
    
    private let carAge: UILabel = {
        let label = UILabel()
        label.textColor = Colors.textColor
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureSubviews()
        addConstraints()
        selectionStyle = .none
        contentView.isUserInteractionEnabled = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(with model: CarInfoModel) {
        carName.text = model.carName
        carAge.text = model.carAge
        carImage.downloaded(from: model.carImage)
    }
    
    private func configureSubviews() {
        backgroundColor = Colors.backgroundColor
        contentView.addSubview(editButton)
        contentView.addSubview(carImage)
        contentView.addSubview(carName)
        contentView.addSubview(carAge)
    }
    
    private func addConstraints() {
        editButton.translatesAutoresizingMaskIntoConstraints = false
        carImage.translatesAutoresizingMaskIntoConstraints = false
        carAge.translatesAutoresizingMaskIntoConstraints = false
        carName.translatesAutoresizingMaskIntoConstraints = false
        
        
        NSLayoutConstraint.activate([
            carImage.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            carImage.leftAnchor.constraint(equalTo: leftAnchor, constant: 20),
            carImage.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20),
            carImage.heightAnchor.constraint(equalToConstant: 130),
            carImage.widthAnchor.constraint(equalToConstant: 170),
            
            carName.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            carName.rightAnchor.constraint(equalTo: rightAnchor, constant: -20),
            
            carAge.topAnchor.constraint(equalTo: carName.bottomAnchor, constant: 20),
            carAge.rightAnchor.constraint(equalTo: rightAnchor, constant: -20),
            
            editButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -20),
            editButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant:  -20),
            editButton.widthAnchor.constraint(equalToConstant: 155),
            editButton.heightAnchor.constraint(equalToConstant: 30)
        ])
    }
}

extension UIImageView {
    
    func downloaded(from url: URL, contentMode mode: ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    
}
