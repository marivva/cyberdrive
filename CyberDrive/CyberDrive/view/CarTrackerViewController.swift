//
//  CarTrackerViewController.swift
//  CyberDrive
//
//  Created by Gala on 18.12.2021.
//

import UIKit

final class CarTrackerViewController: UIViewController {
    
    private let tableView = UITableView()
    
    private let presenter: CarTrackerPresenter
    
    private var model: CarTrackerModel?
    
    init(presenter: CarTrackerPresenter) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) is not supported")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Colors.backgroundColor
        
        configureSubviews()
        model = presenter.getModel()
    }
    
    private func configureSubviews() {
        configureNavBar()
        configureTableView()
        setupConstraints()
    }
    
    private func configureTableView() {
        view.addSubview(tableView)
        tableView.backgroundColor = Colors.backgroundColor
        tableView.delaysContentTouches = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(CarInfoTableViewCell.self, forCellReuseIdentifier: CarInfoTableViewCell.identifier)
        tableView.register(NotificationTableViewCell.self, forCellReuseIdentifier: NotificationTableViewCell.identifier)
        tableView.register(NotPickerTableViewCell.self, forCellReuseIdentifier: NotPickerTableViewCell.identifier)
    }
    
    private func configureNavBar() {
        navigationController?.tabBarController?.tabBar.tintColor = Colors.textColor
        navigationController?.tabBarController?.tabBar.backgroundColor = Colors.backgroundColor
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: Colors.textColor]
        navigationController?.navigationBar.backgroundColor = Colors.backgroundColor
        navigationController?.navigationBar.topItem?.title = "Мой автомобиль"
        navigationController?.navigationBar.topItem?.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addNewCar))
    }
    
    private func setupConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    
    @objc private func addNewCar() {
        // TODO
        // действие по нажатию на плюсик вверху
    }
}

extension CarTrackerViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 156
        } else if indexPath.row == 0 {
            return 40
        } else {
            return 95
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return (model?.notificationModel.count ?? 0) + 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = CarInfoTableViewCell()
            
            if let model = model {
                cell.setup(with: model.carInfoModel)
            }
            return cell
        } else if indexPath.row == 0 {
            return NotPickerTableViewCell()
        } else {
            let cell = NotificationTableViewCell()
            if let model = model {
                cell.setup(with: model.notificationModel[indexPath.row - 1])
            }
            return cell
        }
    }
    
    
}
