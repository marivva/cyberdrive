//
//  CarTrackerPresenter.swift
//  CyberDrive
//
//  Created by Gala on 18.12.2021.
//

import Foundation

final class CarTrackerPresenter {
    
    static let mockCarInfoModel = CarInfoModel(carImage: URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRvZh7F1R5QhHfSMkRJOorfVfnZTrDuBFR5oQ&usqp=CAU")!,
                                               carName: "Devyatka",
                                               carAge: "100 km")
    
    static let mockNotificationModel1 = NotificationModel(notificationDate: Date(),
                                                          notificationMessage: "Change something 1",
                                                          notificationStatus: true)
    
    static let mockNotificationModel2 = NotificationModel(notificationDate: Date(),
                                                          notificationMessage: "Change something 2",
                                                          notificationStatus: false)
     
    func getModel() -> CarTrackerModel? {
        // TODO: идем в бд, достаем данные, преобразуем в модель и возвращаем ее вместо моков
        return CarTrackerModel(carInfoModel: CarTrackerPresenter.mockCarInfoModel,
                               notificationModel: [CarTrackerPresenter.mockNotificationModel1,
                                                   CarTrackerPresenter.mockNotificationModel2])
    }
}
