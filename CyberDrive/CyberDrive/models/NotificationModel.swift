//
//  NotificationModel.swift
//  CyberDrive
//
//  Created by Gala on 18.12.2021.
//

import Foundation

struct NotificationModel {
    let notificationDate: Date
    let notificationMessage: String
    let notificationStatus: Bool
}

