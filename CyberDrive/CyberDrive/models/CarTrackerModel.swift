//
//  CarTrackerModel.swift
//  CyberDrive
//
//  Created by Gala on 18.12.2021.
//

struct CarTrackerModel {
    let carInfoModel: CarInfoModel
    let notificationModel: [NotificationModel]
}
