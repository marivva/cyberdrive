//
//  TabBarItems.swift
//  CyberDrive
//
//  Created by Maha on 18.12.2021.
//

enum TabBarItems: Int {
    case search = 1
    case tracker
    case profile
}
