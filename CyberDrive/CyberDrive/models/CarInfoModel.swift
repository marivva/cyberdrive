//
//  CarInfoModel.swift
//  CyberDrive
//
//  Created by Maha on 18.12.2021.
//

import Foundation

struct CarInfoModel {
    let carImage: URL
    let carName: String
    let carAge: String
}
