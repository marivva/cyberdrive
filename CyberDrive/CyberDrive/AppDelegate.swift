//
//  AppDelegate.swift
//  CyberDrive
//
//  Created by Gala on 18.12.2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let navigationController = CDTabBarNavigationController()
        
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        return true
    }

}

